---
layout: handbook-page-toc
title: "Security Awards Leaderboard"
---

### On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

This page is [auto-generated and updated every Mondays](../security-awards-program.html#process).

# Leaderboard FY23

## Yearly

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@leipert](https://gitlab.com/leipert) | 1 | 2000 |
| [@tkuah](https://gitlab.com/tkuah) | 2 | 1510 |
| [@vitallium](https://gitlab.com/vitallium) | 3 | 1200 |
| [@sabrams](https://gitlab.com/sabrams) | 4 | 1100 |
| [@djadmin](https://gitlab.com/djadmin) | 5 | 980 |
| [@bdenkovych](https://gitlab.com/bdenkovych) | 6 | 940 |
| [@hfyngvason](https://gitlab.com/hfyngvason) | 7 | 900 |
| [@brodock](https://gitlab.com/brodock) | 8 | 800 |
| [@kassio](https://gitlab.com/kassio) | 9 | 600 |
| [@sethgitlab](https://gitlab.com/sethgitlab) | 10 | 600 |
| [@cam_swords](https://gitlab.com/cam_swords) | 11 | 600 |
| [@.luke](https://gitlab.com/.luke) | 12 | 560 |
| [@Andysoiron](https://gitlab.com/Andysoiron) | 13 | 540 |
| [@ratchade](https://gitlab.com/ratchade) | 14 | 500 |
| [@rossfuhrman](https://gitlab.com/rossfuhrman) | 15 | 500 |
| [@jlear](https://gitlab.com/jlear) | 16 | 500 |
| [@acunskis](https://gitlab.com/acunskis) | 17 | 500 |
| [@peterhegman](https://gitlab.com/peterhegman) | 18 | 500 |
| [@sgoldstein](https://gitlab.com/sgoldstein) | 19 | 500 |
| [@atiwari71](https://gitlab.com/atiwari71) | 20 | 500 |
| [@kerrizor](https://gitlab.com/kerrizor) | 21 | 480 |
| [@cwoolley-gitlab](https://gitlab.com/cwoolley-gitlab) | 22 | 460 |
| [@garyh](https://gitlab.com/garyh) | 23 | 440 |
| [@jerasmus](https://gitlab.com/jerasmus) | 24 | 440 |
| [@georgekoltsov](https://gitlab.com/georgekoltsov) | 25 | 400 |
| [@toupeira](https://gitlab.com/toupeira) | 26 | 400 |
| [@stanhu](https://gitlab.com/stanhu) | 27 | 400 |
| [@xanf](https://gitlab.com/xanf) | 28 | 400 |
| [@rcobb](https://gitlab.com/rcobb) | 29 | 300 |
| [@viktomas](https://gitlab.com/viktomas) | 30 | 300 |
| [@joe-shaw](https://gitlab.com/joe-shaw) | 31 | 300 |
| [@ajwalker](https://gitlab.com/ajwalker) | 32 | 300 |
| [@m_frankiewicz](https://gitlab.com/m_frankiewicz) | 33 | 200 |
| [@dbalexandre](https://gitlab.com/dbalexandre) | 34 | 200 |
| [@vshushlin](https://gitlab.com/vshushlin) | 35 | 200 |
| [@alberts-gitlab](https://gitlab.com/alberts-gitlab) | 36 | 200 |
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 37 | 150 |
| [@pshutsin](https://gitlab.com/pshutsin) | 38 | 140 |
| [@dskim_gitlab](https://gitlab.com/dskim_gitlab) | 39 | 140 |
| [@furkanayhan](https://gitlab.com/furkanayhan) | 40 | 140 |
| [@10io](https://gitlab.com/10io) | 41 | 130 |
| [@alexpooley](https://gitlab.com/alexpooley) | 42 | 130 |
| [@egrieff](https://gitlab.com/egrieff) | 43 | 120 |
| [@ifarkas](https://gitlab.com/ifarkas) | 44 | 120 |
| [@mattkasa](https://gitlab.com/mattkasa) | 45 | 100 |
| [@mwoolf](https://gitlab.com/mwoolf) | 46 | 90 |
| [@digitalmoksha](https://gitlab.com/digitalmoksha) | 47 | 90 |
| [@drew](https://gitlab.com/drew) | 48 | 90 |
| [@dmakovey](https://gitlab.com/dmakovey) | 49 | 80 |
| [@mbobin](https://gitlab.com/mbobin) | 50 | 80 |
| [@rmarshall](https://gitlab.com/rmarshall) | 51 | 80 |
| [@seanarnold](https://gitlab.com/seanarnold) | 52 | 80 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 53 | 60 |
| [@proglottis](https://gitlab.com/proglottis) | 54 | 60 |
| [@minac](https://gitlab.com/minac) | 55 | 60 |
| [@hortiz5](https://gitlab.com/hortiz5) | 56 | 60 |
| [@pedropombeiro](https://gitlab.com/pedropombeiro) | 57 | 60 |
| [@lauraX](https://gitlab.com/lauraX) | 58 | 60 |
| [@nick.thomas](https://gitlab.com/nick.thomas) | 59 | 60 |
| [@bala.kumar](https://gitlab.com/bala.kumar) | 60 | 60 |
| [@dblessing](https://gitlab.com/dblessing) | 61 | 60 |
| [@cngo](https://gitlab.com/cngo) | 62 | 60 |
| [@avielle](https://gitlab.com/avielle) | 63 | 60 |
| [@allison.browne](https://gitlab.com/allison.browne) | 64 | 60 |
| [@serenafang](https://gitlab.com/serenafang) | 65 | 60 |
| [@fabiopitino](https://gitlab.com/fabiopitino) | 66 | 60 |
| [@ebaque](https://gitlab.com/ebaque) | 67 | 50 |
| [@ghickey](https://gitlab.com/ghickey) | 68 | 40 |
| [@felipe_artur](https://gitlab.com/felipe_artur) | 69 | 40 |
| [@balasankarc](https://gitlab.com/balasankarc) | 70 | 40 |
| [@manojmj](https://gitlab.com/manojmj) | 71 | 40 |
| [@ohoral](https://gitlab.com/ohoral) | 72 | 30 |
| [@subashis](https://gitlab.com/subashis) | 73 | 30 |
| [@brytannia](https://gitlab.com/brytannia) | 74 | 30 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 75 | 30 |
| [@robotmay_gitlab](https://gitlab.com/robotmay_gitlab) | 76 | 30 |
| [@shinya.maeda](https://gitlab.com/shinya.maeda) | 77 | 30 |
| [@ealcantara](https://gitlab.com/ealcantara) | 78 | 30 |
| [@fjsanpedro](https://gitlab.com/fjsanpedro) | 79 | 30 |
| [@ntepluhina](https://gitlab.com/ntepluhina) | 80 | 20 |
| [@mikolaj_wawrzyniak](https://gitlab.com/mikolaj_wawrzyniak) | 81 | 20 |
| [@terrichu](https://gitlab.com/terrichu) | 82 | 20 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@greg](https://gitlab.com/greg) | 1 | 500 |
| [@godfat-gitlab](https://gitlab.com/godfat-gitlab) | 2 | 500 |
| [@jacobvosmaer-gitlab](https://gitlab.com/jacobvosmaer-gitlab) | 3 | 440 |
| [@f_santos](https://gitlab.com/f_santos) | 4 | 300 |
| [@skarbek](https://gitlab.com/skarbek) | 5 | 300 |
| [@andrewn](https://gitlab.com/andrewn) | 6 | 200 |
| [@katiemacoy](https://gitlab.com/katiemacoy) | 7 | 50 |
| [@rspeicher](https://gitlab.com/rspeicher) | 8 | 30 |
| [@john.mcdonnell](https://gitlab.com/john.mcdonnell) | 9 | 30 |
| [@fneill](https://gitlab.com/fneill) | 10 | 30 |

### Non-Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@kmcknight](https://gitlab.com/kmcknight) | 1 | 500 |
| [@NicoleSchwartz](https://gitlab.com/NicoleSchwartz) | 2 | 400 |
| [@mbruemmer](https://gitlab.com/mbruemmer) | 3 | 400 |
| [@vburton](https://gitlab.com/vburton) | 4 | 30 |

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@rpadovani](https://gitlab.com/rpadovani) | 1 | 1200 |
| [@feistel](https://gitlab.com/feistel) | 2 | 400 |
| [@spirosoik](https://gitlab.com/spirosoik) | 3 | 300 |
| [@benyanke](https://gitlab.com/benyanke) | 4 | 200 |
| [@zined](https://gitlab.com/zined) | 5 | 200 |
| [@trakos](https://gitlab.com/trakos) | 6 | 200 |

## FY23-Q2

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@sabrams](https://gitlab.com/sabrams) | 1 | 1100 |
| [@bdenkovych](https://gitlab.com/bdenkovych) | 2 | 900 |
| [@djadmin](https://gitlab.com/djadmin) | 3 | 480 |
| [@viktomas](https://gitlab.com/viktomas) | 4 | 300 |
| [@joe-shaw](https://gitlab.com/joe-shaw) | 5 | 300 |
| [@ajwalker](https://gitlab.com/ajwalker) | 6 | 300 |
| [@dbalexandre](https://gitlab.com/dbalexandre) | 7 | 200 |
| [@brodock](https://gitlab.com/brodock) | 8 | 200 |
| [@vshushlin](https://gitlab.com/vshushlin) | 9 | 200 |
| [@alberts-gitlab](https://gitlab.com/alberts-gitlab) | 10 | 200 |
| [@toupeira](https://gitlab.com/toupeira) | 11 | 160 |
| [@furkanayhan](https://gitlab.com/furkanayhan) | 12 | 110 |
| [@drew](https://gitlab.com/drew) | 13 | 90 |
| [@dskim_gitlab](https://gitlab.com/dskim_gitlab) | 14 | 80 |
| [@kerrizor](https://gitlab.com/kerrizor) | 15 | 80 |
| [@seanarnold](https://gitlab.com/seanarnold) | 16 | 80 |
| [@digitalmoksha](https://gitlab.com/digitalmoksha) | 17 | 60 |
| [@egrieff](https://gitlab.com/egrieff) | 18 | 60 |
| [@avielle](https://gitlab.com/avielle) | 19 | 60 |
| [@allison.browne](https://gitlab.com/allison.browne) | 20 | 60 |
| [@tkuah](https://gitlab.com/tkuah) | 21 | 60 |
| [@serenafang](https://gitlab.com/serenafang) | 22 | 60 |
| [@fabiopitino](https://gitlab.com/fabiopitino) | 23 | 60 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 24 | 40 |
| [@manojmj](https://gitlab.com/manojmj) | 25 | 40 |
| [@alexpooley](https://gitlab.com/alexpooley) | 26 | 30 |
| [@mwoolf](https://gitlab.com/mwoolf) | 27 | 30 |
| [@mbobin](https://gitlab.com/mbobin) | 28 | 30 |
| [@fjsanpedro](https://gitlab.com/fjsanpedro) | 29 | 30 |
| [@10io](https://gitlab.com/10io) | 30 | 30 |
| [@terrichu](https://gitlab.com/terrichu) | 31 | 20 |
| [@ifarkas](https://gitlab.com/ifarkas) | 32 | 20 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@godfat-gitlab](https://gitlab.com/godfat-gitlab) | 1 | 500 |
| [@jacobvosmaer-gitlab](https://gitlab.com/jacobvosmaer-gitlab) | 2 | 40 |
| [@fneill](https://gitlab.com/fneill) | 3 | 30 |

### Non-Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@kmcknight](https://gitlab.com/kmcknight) | 1 | 500 |
| [@mbruemmer](https://gitlab.com/mbruemmer) | 2 | 400 |

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@trakos](https://gitlab.com/trakos) | 1 | 200 |

## FY23-Q1

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@leipert](https://gitlab.com/leipert) | 1 | 2000 |
| [@tkuah](https://gitlab.com/tkuah) | 2 | 1450 |
| [@vitallium](https://gitlab.com/vitallium) | 3 | 1200 |
| [@hfyngvason](https://gitlab.com/hfyngvason) | 4 | 900 |
| [@kassio](https://gitlab.com/kassio) | 5 | 600 |
| [@brodock](https://gitlab.com/brodock) | 6 | 600 |
| [@sethgitlab](https://gitlab.com/sethgitlab) | 7 | 600 |
| [@cam_swords](https://gitlab.com/cam_swords) | 8 | 600 |
| [@.luke](https://gitlab.com/.luke) | 9 | 560 |
| [@Andysoiron](https://gitlab.com/Andysoiron) | 10 | 540 |
| [@ratchade](https://gitlab.com/ratchade) | 11 | 500 |
| [@rossfuhrman](https://gitlab.com/rossfuhrman) | 12 | 500 |
| [@jlear](https://gitlab.com/jlear) | 13 | 500 |
| [@acunskis](https://gitlab.com/acunskis) | 14 | 500 |
| [@peterhegman](https://gitlab.com/peterhegman) | 15 | 500 |
| [@sgoldstein](https://gitlab.com/sgoldstein) | 16 | 500 |
| [@atiwari71](https://gitlab.com/atiwari71) | 17 | 500 |
| [@djadmin](https://gitlab.com/djadmin) | 18 | 500 |
| [@cwoolley-gitlab](https://gitlab.com/cwoolley-gitlab) | 19 | 460 |
| [@garyh](https://gitlab.com/garyh) | 20 | 440 |
| [@jerasmus](https://gitlab.com/jerasmus) | 21 | 440 |
| [@georgekoltsov](https://gitlab.com/georgekoltsov) | 22 | 400 |
| [@stanhu](https://gitlab.com/stanhu) | 23 | 400 |
| [@kerrizor](https://gitlab.com/kerrizor) | 24 | 400 |
| [@xanf](https://gitlab.com/xanf) | 25 | 400 |
| [@rcobb](https://gitlab.com/rcobb) | 26 | 300 |
| [@toupeira](https://gitlab.com/toupeira) | 27 | 240 |
| [@m_frankiewicz](https://gitlab.com/m_frankiewicz) | 28 | 200 |
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 29 | 150 |
| [@pshutsin](https://gitlab.com/pshutsin) | 30 | 140 |
| [@10io](https://gitlab.com/10io) | 31 | 100 |
| [@mattkasa](https://gitlab.com/mattkasa) | 32 | 100 |
| [@alexpooley](https://gitlab.com/alexpooley) | 33 | 100 |
| [@ifarkas](https://gitlab.com/ifarkas) | 34 | 100 |
| [@dmakovey](https://gitlab.com/dmakovey) | 35 | 80 |
| [@rmarshall](https://gitlab.com/rmarshall) | 36 | 80 |
| [@mwoolf](https://gitlab.com/mwoolf) | 37 | 60 |
| [@dskim_gitlab](https://gitlab.com/dskim_gitlab) | 38 | 60 |
| [@proglottis](https://gitlab.com/proglottis) | 39 | 60 |
| [@minac](https://gitlab.com/minac) | 40 | 60 |
| [@egrieff](https://gitlab.com/egrieff) | 41 | 60 |
| [@hortiz5](https://gitlab.com/hortiz5) | 42 | 60 |
| [@pedropombeiro](https://gitlab.com/pedropombeiro) | 43 | 60 |
| [@lauraX](https://gitlab.com/lauraX) | 44 | 60 |
| [@nick.thomas](https://gitlab.com/nick.thomas) | 45 | 60 |
| [@bala.kumar](https://gitlab.com/bala.kumar) | 46 | 60 |
| [@dblessing](https://gitlab.com/dblessing) | 47 | 60 |
| [@cngo](https://gitlab.com/cngo) | 48 | 60 |
| [@ebaque](https://gitlab.com/ebaque) | 49 | 50 |
| [@mbobin](https://gitlab.com/mbobin) | 50 | 50 |
| [@ghickey](https://gitlab.com/ghickey) | 51 | 40 |
| [@bdenkovych](https://gitlab.com/bdenkovych) | 52 | 40 |
| [@felipe_artur](https://gitlab.com/felipe_artur) | 53 | 40 |
| [@balasankarc](https://gitlab.com/balasankarc) | 54 | 40 |
| [@ohoral](https://gitlab.com/ohoral) | 55 | 30 |
| [@subashis](https://gitlab.com/subashis) | 56 | 30 |
| [@brytannia](https://gitlab.com/brytannia) | 57 | 30 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 58 | 30 |
| [@furkanayhan](https://gitlab.com/furkanayhan) | 59 | 30 |
| [@robotmay_gitlab](https://gitlab.com/robotmay_gitlab) | 60 | 30 |
| [@shinya.maeda](https://gitlab.com/shinya.maeda) | 61 | 30 |
| [@digitalmoksha](https://gitlab.com/digitalmoksha) | 62 | 30 |
| [@ealcantara](https://gitlab.com/ealcantara) | 63 | 30 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 64 | 20 |
| [@ntepluhina](https://gitlab.com/ntepluhina) | 65 | 20 |
| [@mikolaj_wawrzyniak](https://gitlab.com/mikolaj_wawrzyniak) | 66 | 20 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@greg](https://gitlab.com/greg) | 1 | 500 |
| [@jacobvosmaer-gitlab](https://gitlab.com/jacobvosmaer-gitlab) | 2 | 400 |
| [@f_santos](https://gitlab.com/f_santos) | 3 | 300 |
| [@skarbek](https://gitlab.com/skarbek) | 4 | 300 |
| [@andrewn](https://gitlab.com/andrewn) | 5 | 200 |
| [@katiemacoy](https://gitlab.com/katiemacoy) | 6 | 50 |
| [@rspeicher](https://gitlab.com/rspeicher) | 7 | 30 |
| [@john.mcdonnell](https://gitlab.com/john.mcdonnell) | 8 | 30 |

### Non-Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@NicoleSchwartz](https://gitlab.com/NicoleSchwartz) | 1 | 400 |
| [@vburton](https://gitlab.com/vburton) | 2 | 30 |

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@rpadovani](https://gitlab.com/rpadovani) | 1 | 1200 |
| [@feistel](https://gitlab.com/feistel) | 2 | 400 |
| [@spirosoik](https://gitlab.com/spirosoik) | 3 | 300 |
| [@benyanke](https://gitlab.com/benyanke) | 4 | 200 |
| [@zined](https://gitlab.com/zined) | 5 | 200 |


